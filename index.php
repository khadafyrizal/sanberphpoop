<?php

require ('animal.php');
require ('frog.php');
require ('ape.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        $objek = new Animal("Shaun");

        echo "Nama Hewan      : ".$objek->name."<br>";
        echo "Jumlah Kaki     : ".$objek->legs."<br>";
        echo "Berdarah Dingin : ".$objek->cold_blooded."<br> <br>";
        
        $objek2 = new Frog("Katak");

        echo "Nama Hewan      : ".$objek2->name."<br>";
        echo "Jumlah Kaki     : ".$objek2->legs."<br>";
        echo "Berdarah Dingin : ".$objek2->cold_blooded."<br>";
        $objek2->jump();

        $objek3 = new Ape("Monyet");
        echo "Nama Hewan      : ".$objek3->name."<br>";
        echo "Jumlah Kaki     : ".$objek3->legs."<br>";
        echo "Berdarah Dingin : ".$objek3->cold_blooded."<br>";
        $objek3->yell();
        ?>
</body>
</html>